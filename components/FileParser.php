<?php

namespace app\components;

use Yii;
use Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileParser
 *
 * @author fyl
 */
class FileParser {

    //put your code here
    static function parse($class) {
        $model = new $class;
        if (!($model instanceof FileParserInterface)) {
            throw new Exception('Неверный класс');
        }
        $fp = fopen($class::getFileName(), 'r');//открытие файла
        $data = [];
        if ($fp) {
            $colname = self::getArrayFromStr(fgets($fp));// Первая строчка в файле, имена колонок
            while (!feof($fp)) {
                $str = fgets($fp);
                if ($str) {
                    $data = self::getArrayFromStr($str);
                    Yii::$app->db->createCommand()->insert($class::tableName(), self::getInsertArray($class::getLabelsArray(),$colname, $data))->execute();//добавление в таблицу
                }
            }
        } else {
            throw new Exception('ошибка открытия файла');
        }
        fclose($fp);
    }
/**
 * 
 * @param type $str
 * @return typeПреобразует строку из файла в массив
 */
    static function getArrayFromStr($str) {
        return explode("\t", str_replace(["\r", "\n"], '', $str));
    }
/**
 * Возвращает массив необходимый для вставки в таблицу
 * @param type $labels
 * @param type $colname
 * @param type $row
 * @return type
 */
    static function getInsertArray($labels,$colname, $row) {
        $insert = [];
        $size = count($colname);
        for ($i = 0; $i < $size; $i++) {
            $insert[$labels[$colname[$i]]] = $row[$i];
        }
        return $insert;
    }

}
