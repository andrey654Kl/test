<?php
namespace app\components;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileParserInterface
 *
 * @author fyl
 */
interface FileParserInterface {
    //put your code here
   /**
    * Возвращает путь к файлу из которого берутся данные для таблицы
    */
   static function getFileName();
   /**
    * Возвращает массив соотношения столбцов из файла и название таблиц в бд
    */
   static function getLabelsArray();
}
