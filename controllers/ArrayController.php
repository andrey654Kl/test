<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ArrayForm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArrayController
 *
 * @author fyl
 */
class ArrayController extends Controller {

    //put your code here

    public function actionIndex() {
        $model = new ArrayForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $result = $model->handler();
            return $this->render('result', [
                        'result' => $result,
            ]);
        }
        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    function actionAddField() {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_arrayField', []);
        }
    }

}
