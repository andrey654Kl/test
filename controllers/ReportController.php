<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ArrayForm;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportController
 *
 * @author fyl
 */
class ReportController extends Controller{
    //put your code here
    
   function actionAgency(){
        $model = new \app\models\AgencyReportForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->render('agency_result', [
                        'model' => $model,
            ]);
        }
        return $this->render('agency_form', [
                    'model' => $model,
        ]);
   }
}
