<?php

use yii\db\Migration;

class m161101_142653_add_network extends Migration
{
    public function up()
    {
          $this->createTable('agency_network', [
              'id'=>$this->primaryKey(),
              'name'=>$this->string(255)->notNull()
          ]);
    }

    public function down()
    {
        $this->dropTable('agency_network');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
