<?php

use yii\db\Migration;

class m161101_142702_add_agency extends Migration {

    public function up() {
        $this->createTable('agency', [
            'id' => $this->primaryKey(),
            'networkID'=>$this->integer()->notNull(),
            'name' => $this->string(255)->notNull()
        ]);
    }

    public function down() {
        $this->dropTable('agency');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
