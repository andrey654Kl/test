<?php

use yii\db\Migration;

class m161101_142710_add_billing extends Migration
{
    public function up()
    {
  $this->createTable('agency_billing', [
            'id' => $this->primaryKey(),
            'agencyID'=>$this->integer()->notNull(),
            'userID' => $this->integer()->notNull(),
            'date'=>$this->dateTime(),
            'amount'=>$this->decimal(6, 2),
        ]);
    }

    public function down()
    {
        $this->dropTable('agency_billing');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
