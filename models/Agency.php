<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agency".
 *
 * @property integer $id
 * @property integer $networkID
 * @property string $name
 */
class Agency extends \yii\db\ActiveRecord implements \app\components\FileParserInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['networkID', 'name'], 'required'],
            [['networkID'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'networkID' => 'Network ID',
            'name' => 'Name',
        ];
    }

    public static function getFileName() {
         return Yii::getAlias('@app').'/web/agency.txt';
    }

    public static function getLabelsArray() {
        return [
            'agency_id'=>'id',
            'agency_network_id'=>'networkID',
            'agency_name'=>'name'
        ];
    }

}
