<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agency_billing".
 *
 * @property integer $id
 * @property integer $agencyID
 * @property integer $userID
 * @property string $date
 * @property string $amount
 */
class AgencyBilling extends \yii\db\ActiveRecord implements \app\components\FileParserInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency_billing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agencyID', 'userID'], 'required'],
            [['agencyID', 'userID'], 'integer'],
            [['date'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agencyID' => 'Agency ID',
            'userID' => 'User ID',
            'date' => 'Date',
            'amount' => 'Amount',
        ];
    }


    public static function getFileName() {
        return Yii::getAlias('@app').'/web/billing.txt';
    }

    public static function getLabelsArray() {
         return [
             'agency_id'=>'agencyID',
             'user_id'=>'userID',
             'date'=>'date',
             'amount'=>'amount'];
    }

}
