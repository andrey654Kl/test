<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agency_network".
 *
 * @property integer $id
 * @property string $name
 */
class AgencyNetwork extends \yii\db\ActiveRecord implements \app\components\FileParserInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency_network';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }


    public static function getFileName() {
        return Yii::getAlias('@app').'/web/agency_network.txt';
    }

    public static function getLabelsArray() {
        return [
            'agency_network_id'=>'id',
            'agency_network_name'=>'name',
        ];
    }

}
