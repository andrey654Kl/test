<?php

namespace app\models;

use Yii;
use yii\base\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AgencyReportForm
 *
 * @author fyl
 */
class AgencyReportForm extends Model {

    //put your code here
    public $startDate;//Начало периода
    public $endDate;//Конец периода

    public function rules() {
        return [
            [['startDate', 'endDate'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'startDate' => 'Начало периода',
            'endDate' => 'Конец периода'
        ];
    }
// Возвращает данные для отчета
    function getData() {
        $return = [];
        $startDate = date('Y-m-d ', strtotime($this->startDate));// Преобразование даты в нужный формат
        $endDate = date('Y-m-d ', strtotime($this->endDate));
        $data = Agency::find()
                ->alias('a')
                ->select('n.name as netname,a.name as agname,sum(b.amount) as sum')
                ->leftJoin(['n' => AgencyNetwork::tableName()], 'a.networkID = n.id')
                ->leftJoin(['b' => AgencyBilling::tableName()], 'b.agencyID = a.id and b.date >=:startdate and b.date <=:enddate',[':startdate'=>$startDate,':enddate'=>$endDate])
                ->groupBy('a.id')
                ->asArray()
                ->all();
        $totalCount = 0;
        foreach((array)$data as $agency){ // Считаем общую стоимость 
            $totalCount+=$agency['sum'];
        }
        $return['data'] = $data;
        $return['total'] = $totalCount;
        return $return;
    }

}
