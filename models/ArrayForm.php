<?php

namespace app\models;

use Yii;
use yii\base\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArrayForm
 *
 * @author fyl
 */
class ArrayForm extends Model {

    //put your code here
    public $A;

    public function rules() {
        return [
            ['A', 'validateElements'],
            ['A', 'validateCountArray'],
        ];
    }

    public function handler() {
        $array = $this->A;
        $f = $array[0];//Сумма первой части массива(для p=1 равно значению первого элемента массива)
        $e = array_sum($array) - $f; // Сумма второй части массива (для p=1 равно сумме всех, кроме первого, элементов массива)
        $minD = $this->getD($f, $e); 
        for ($p = 1; $p < count($array); $p++) {
            $f+=$array[$p];//Увеличиваем сумму первой части массива на p элемент массива
            $e-=$array[$p];//Уменьшение суммы второй части массива на p элемент массива
            $mod = $this->getD($f, $e);
            if ($mod < $minD) {
                $minD = $mod;
            }
        }
        return $minD;
    }
/**
 * Проверка корректности элементов
 * @param type $attribute
 * @param type $params
 */
    function validateElements($attribute, $params) {
        foreach ((array) $this->A as $element) {
            if (!is_numeric($element) || $element < -1000 || $element > 1000) {
                $this->addError('A', 'Одно из полей имеет некорректное значение');
            }
        }
    }
/**
 * Проверка на правильность размерности массива
 * @param type $attribute
 * @param type $params
 */
    function validateCountArray($attribute, $params) {
        $countElements = count($this->A);
        if ($countElements <= 1) {
            $this->addError('A', 'Добавьте и заполните как минимум два поля');
        }
        if ($countElements > 10) {
            $this->addError('A', 'Большое количество элементов, удалите лишнее');
        }
    }

    function beforeValidate() {
        if (parent::beforeValidate()) {
            $this->formatEmptyValue();
            return true;
        }
        return false;
    }
/**
 * Замена пустых элементов массива на 0
 */
    function formatEmptyValue() {
        foreach ((array) $this->A as $key=>$element) {
            if ($element === "") {
                $this->A[$key] = 0;
            }
        }
    }
/**
 * Функция для получения абсолютной разности сум массивов
 * @param type $valueOne
 * @param type $valueTwo
 * @return type
 */
    private function getD($valueOne, $valueTwo) {
        return abs($valueOne - $valueTwo);
    }

}
