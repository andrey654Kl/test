<?php

use yii\helpers\Html;

$model = isset($model) ? $model : new \app\models\ArrayForm;
$field = isset($key) ? 'A['.$key.']' : 'A[]';
?>
<div class="array_field" style="padding: 5px;" >

    <?= Html::activeInput('number', $model, $field, ['min' => -1000,'max'=>1000,'style'=>'width:200px']) ?>
    <?= yii\bootstrap\Html::button('Удалить поле', ['class' => 'remove_array_value']) ?>
</div>
