<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Форма';
$this->params['breadcrumbs'][] = $this->title;
$countElements = is_array($model->A) ? count($model->A) : 3;
?>
<div class="array-index">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php
    $form = ActiveForm::begin([
                'id' => 'array-form',
    ]);
    echo $form->errorSummary([$model]);
    ?>
    <div id="array_fields">
        <?php
        for ($i = 0; $i < $countElements; $i++) {
            echo $this->render('_arrayField', ['model' => $model, 'key' => $i]);
        }
        ?>
    </div>
    <div style="padding:10px">
        <?= Html::a('Добавить поле', '#', ['id' => 'add_array_field', 'class' => "btn btn-primary btn-sm"]) ?>
    </div>
    <div class="form-group" style="padding:10px">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success', 'name' => 'array-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    const MAX_COUNT = 10;
    $("#add_array_field").click(function() {
        var num = getCurrentCountFields();
        if (num >= MAX_COUNT)
            return false;
        getAjaxContent();
        return false;
    });
    $('#array_fields').on('click', '.remove_array_value', function() {
        $(this).parents('.array_field').remove();
        checkCountField();
        return false;
    })
    function getAjaxContent() {

        $.ajax({
            url: '<?= Url::to(['add-field']) ?>',
//            dataType: 'json',
            type: 'post',
            success: function(result) {
                $("#array_fields").append(result);
                checkCountField();
            }
        });
    }
    function getCurrentCountFields() {
        return $('.array_field').length;
    }

    function checkCountField() {
        var num = getCurrentCountFields();
        if (num >= MAX_COUNT) {
            $("#add_array_field").hide();
        } else {
            $("#add_array_field").show();
        }
    }
</script>