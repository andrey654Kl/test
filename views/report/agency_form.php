<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Отчет по агентствам';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('files/daterange/moment.js');
$this->registerJsFile('files/daterange/daterangepicker.js');
$this->registerCssFile('files/daterange/daterangepicker.css');
$this->registerJs("$(function(){
	$('#startDate,#endDate').daterangepicker({
		singleDatePicker: true,
                locale: {
			format: 'DD.MM.YYYY'
		}
	});
});");
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
    ]);
    ?>

<?= $form->field($model, 'startDate')->textInput(['id' => 'startDate']) ?>

        <?= $form->field($model, 'endDate')->textInput(['id' => 'endDate']) ?>


    <div class="form-group">
<?= Html::submitButton('Создать', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
