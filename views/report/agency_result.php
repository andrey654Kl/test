<?php

use yii\helpers\Html;
$result = $model->getData();
$this->title = 'Отчет с '.$model->startDate.' по '.$model->endDate;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table">
        <tr>
            <th>Название сети</th>
            <th>Название агентства</th>
            <th>Сумма по агентству</th>
        </tr>
        <?php foreach ((array) $result['data'] as $row): ?>
            <tr>
                <td><?= $row['netname'] ?></td>
                <td><?= $row['agname'] ?></td>
                <td><?= ($row['sum'] ? :0) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td><b>Итого</b></td>
            <td></td>
            <td><b><?= $result['total'] ?></b></td>
        </tr>
    </table>

</div>